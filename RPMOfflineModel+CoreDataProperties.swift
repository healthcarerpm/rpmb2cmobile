//
//  RPMOfflineModel+CoreDataProperties.swift
//  
//
//  Created by EHC_Bosch on 20/09/19.
//
//

import Foundation
import CoreData


extension RPMOfflineModel {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<RPMOfflineModel> {
        return NSFetchRequest<RPMOfflineModel>(entityName: "RPMOfflineModel")
    }

    

}
