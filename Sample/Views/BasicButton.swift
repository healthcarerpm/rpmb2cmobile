//
//  BasicButton.swift
//  MyFetal Life
//
//  Created by Om Bhagwan on 19/05/18.
//  Copyright © 2018 Om Bhagwan. All rights reserved.
//

import Foundation
import UIKit

@IBDesignable
class BasicButton: UIButton {
    
    var shadowAdded: Bool = false
    @IBInspectable override var cornerRadius: CGFloat{
        didSet {
            layer.cornerRadius = cornerRadius
            layer.masksToBounds = cornerRadius > 0
        }
    }
    
    @IBInspectable override var borderWidth: CGFloat{
        didSet {
            layer.borderWidth = borderWidth
        }
    }
    
    
    
//    override func draw(_ rect: CGRect) {
//        super.draw(rect)
//
//        if shadowAdded { return }
//        shadowAdded = true
//
//        let shadowLayer = UIView(frame: self.frame)
//        shadowLayer.backgroundColor = UIColor.clear
//        shadowLayer.layer.shadowColor = UIColor.darkGray.cgColor
//        shadowLayer.layer.shadowPath = UIBezierPath(roundedRect: bounds, cornerRadius: self.cornerRadius).cgPath
//        shadowLayer.layer.shadowOffset = CGSize(width: 1.0, height: 1.0)
//        shadowLayer.layer.shadowOpacity = 0.5
//        shadowLayer.layer.shadowRadius = 1
//        shadowLayer.layer.masksToBounds = true
//        shadowLayer.clipsToBounds = false
//
//        self.superview?.addSubview(shadowLayer)
//        self.superview?.bringSubview(toFront: self)
//    }
}

