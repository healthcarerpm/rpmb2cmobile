//
//  LoginViewController.swift
//  Sample
//
//  Created by Isham on 27/12/2018.
//  Copyright © 2018 Isham. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController {

    @IBOutlet weak var userNameField: UITextField!
    @IBOutlet weak var passwordField: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    

    @IBAction func attemptLogin(_ sender: Any) {
        self.showSpinner(onView: self.view)
        if userNameField.text?.isEmpty ?? true {
           return
        }else if passwordField.text?.isEmpty ?? true {
            return
        }
        NetworkManager().login(username: userNameField.text!, password: passwordField.text!) { (response) -> () in
           self.performSegue(withIdentifier: "dashboard", sender: nil)
            self.removeSpinner()
        }
        
        //Test
//        self.performSegue(withIdentifier: "dashboard", sender: nil)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    @IBAction func onForgotPasswordClicked(_ sender: Any) {
    }
    

}
