//
//  LoginViewController.swift
//  Sample
//
//  Created by Isham on 27/12/2018.
//  Copyright © 2018 Isham. All rights reserved.
//

import UIKit
import CoreData

class LoginViewController: UIViewController {

    @IBOutlet weak var userNameField: UITextField!
    @IBOutlet weak var passwordField: UITextField!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var parentView: UIView!
    
    @IBOutlet weak var forgotPasswordLabel: UIButton!
    @IBInspectable var cornerRadius: CGFloat = 2
    
    @IBOutlet weak var descriptionView: UITextView!
    @IBInspectable var shadowOffsetWidth: Int = 0
    @IBInspectable var shadowOffsetHeight: Int = 3
    @IBInspectable var shadowColor: UIColor? = UIColor.black
    @IBInspectable var shadowOpacity: Float = 0.5
    
    var keyboardHeight : CGFloat?
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        let backgroundImage = UIImageView(frame: UIScreen.main.bounds)
        backgroundImage.image = #imageLiteral(resourceName: "bg")
        parentView.insertSubview(backgroundImage, at: 0)

        print(TimeZone.current.identifier)
        userNameField.rightViewMode = UITextField.ViewMode.always
        passwordField.rightViewMode = UITextField.ViewMode.always
        let imageView = UIImageView(frame: CGRect(x: -80, y: 0, width: 5, height: 5))
        let image = UIImage(named: "outline_mail_black")
        imageView.image = image
        imageView.contentMode = .scaleAspectFit
        userNameField.rightView = imageView
        if UserDefaults.standard.value(forKey: "UserID") != nil{
            userNameField.text = UserDefaults.standard.value(forKey: "UserID") as? String
        }
        let passimageView = UIImageView(frame: CGRect(x: -80, y: 0, width: 5, height: 5))
        passimageView.contentMode = .scaleAspectFit
        let passimage = UIImage(named: "outline_lock_black")
        passimageView.image = passimage
        passwordField.rightView = passimageView

        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)

        userNameField.doneAccessory = true
        passwordField.doneAccessory = true
        CoreDataUtils().deleteAllDatabase()
        if #available(iOS 13.0, *) {
                   overrideUserInterfaceStyle = .light
               } else {
                   // Fallback on earlier versions
               }
      
  
    }
    
@objc func keyboardWillShow(notification: NSNotification) {
    keyboardHeight = keyBoardHeight()
    if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
        if self.view.frame.origin.y == 0 {
            self.view.frame.origin.y -= keyboardHeight ?? keyboardSize.height/1.3//keyboardSize.height/2
        }
    }
}

    @IBAction func navigateToRegisterVC(_ sender: UIButton) {
        
        performSegue(withIdentifier: "Register", sender: sender)
                
    }
    @objc func keyboardWillHide(notification: NSNotification) {
    if self.view.frame.origin.y != 0 {
        self.view.frame.origin.y = 0
    }
}
    
    func keyBoardHeight() -> CGFloat {
        return keyboardHeight ?? 150
    }
    

    @IBAction func attemptLogin(_ sender: Any) {
        if userNameField.text?.isEmpty ?? true {
            self.showAlert(title: "Login", message: "Please enter valid username")
           return
        }else if passwordField.text?.isEmpty ?? true {
            self.showAlert(title: "Login", message: "Please enter valid password")
            return
        }
        self.showSpinner(onView: self.view)
        NetworkManager().login(username: userNameField.text!, password: passwordField.text!) { (response) -> () in
            self.removeSpinner(completion: {
                if response != nil {
                    
                    //UserDefaults.standard.set(true, forKey: “userlogin”)
                    let pId = RPMUserSettings.sharedInstance.patientId ?? ""

                    RPMUserSettings.sharedInstance.username = self.userNameField.text!
                    UserDefaults.standard.setValue( RPMUserSettings.sharedInstance.username , forKey: "UserID")
                    
                    NetworkManager().getpatientconsent(patienId: pId, completion: {
                        (status:String) in
//                        if status == "pending" || status == "failure"{
//                               self.performSegue(withIdentifier: "Terms", sender: nil)
//                        }else{
                             self.performSegue(withIdentifier: "LogInToDashboard", sender: nil)
 //                       }
                    })
                    
                }
                else{
                    self.showAlert(title: "Login", message: "Please enter valid username/password")
                }
            })
         
        }
        

    }
    
    func deleteAllRecords() {
        let delegate = UIApplication.shared.delegate as! AppDelegate
        let context = delegate.persistentContainer.viewContext
        
        let deleteFetch = NSFetchRequest<NSFetchRequestResult>(entityName: "User")
        let deleteRequest = NSBatchDeleteRequest(fetchRequest: deleteFetch)
        
        do {
            try context.execute(deleteRequest)
            try context.save()
        } catch {
            print ("There was an error")
        }
    }
    
    
    
    
    @IBAction func onForgotPwdClicked(_ sender: UIButton) {
        
            let alert  = UIAlertController(title: "Warning", message: "Please contact the administrator for your new password or send an email to info@healthcarerpm.com", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
                self.present(alert, animated: true, completion: nil)
    }
    
//    @IBAction func onForgotPasswordClicked(_ sender: Any) {
//        let alert  = UIAlertController(title: "Password Reset", message: "Please contact the administrator for your new password or send an email to info@healthcarerpm.com", preferredStyle: .alert)
//             alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
//             self.present(alert, animated: true, completion: nil)
//        
//    }
    
    @IBAction func onForgotPasswordClicked(_ sender: Any) {
        if UserDefaults.standard.value(forKey: "username") != nil{
            NetworkManager().forgotpassword(params: ["Email":UserDefaults.standard.value(forKey: "username") as! String], completion: {
                _ in
            })
              RPMUserSettings.sharedInstance.username = UserDefaults.standard.value(forKey: "username") as! String
            let alert  = UIAlertController(title: "Password Reset", message: "We have sent an email to your registered email address", preferredStyle: .alert)
             alert.addAction(UIAlertAction(title: "Okay",
                                           style: UIAlertAction.Style.default,
                                  handler: {
                                    (alert: UIAlertAction!) in
                                    self.performSegue(withIdentifier: "ForgotPassword", sender: nil)}
             ))
            self.present(alert, animated: true, completion: nil)
         
        }
        else if self.userNameField.text != ""{
           
            if self.validateInput() == false{
                let alert  = UIAlertController(title: "Password Reset", message: "Please enter a valid email address", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
                self.present(alert, animated: true, completion: nil)
            }else{
                 RPMUserSettings.sharedInstance.username = self.userNameField.text!
                NetworkManager().forgotpassword(params: ["Email": self.userNameField.text!], completion: {
                    _ in
                })
                let alert  = UIAlertController(title: "Password Reset", message: "We have sent an email to your registered email address", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "OK", style: .default, handler: {(
                    alert: UIAlertAction!) in
                    self.performSegue(withIdentifier: "ForgotPassword", sender: nil)}))
                self.present(alert, animated: true, completion: nil)

            }
        }
        else if self.userNameField.text == ""
        {
            let alert  = UIAlertController(title: "Password Reset", message: "Please enter an email address", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
        
    }
    
    func validateInput()->Bool{
        if let email = self.userNameField.text
        {
            if(self.validateEmail(enteredEmail: email)) != true {
                self.showAlert(title: "Info", message: "Invalid Email Id")
                return false
            }
            
        }
        return true
    }
    
    func validateEmail(enteredEmail:String) -> Bool {
        
        let emailFormat = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let emailPredicate = NSPredicate(format:"SELF MATCHES %@", emailFormat)
        return emailPredicate.evaluate(with: enteredEmail)
        
    }
    
    func showAlert (title: String, message: String){
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        self.present(alert, animated: true, completion: nil);
    }
    
    @objc func tapFunction(sender:UITapGestureRecognizer) {
        
        print("tap working")
    }
    
    //Logout
    @IBAction func prepareForUnwind(segue: UIStoryboardSegue) {
        
    }
    

}
