//
//  AilmentSelectionViewController.swift
//  Sample
//
//  Created by Admin on 1/14/20.
//  Copyright © 2020 Isham. All rights reserved.
//

import UIKit

class AilmentSelectionViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    //scroll for the screen
    var scrollView = UIScrollView()
    var patientCondtions = [RPMPatientCondition]()
    //names of patient conditions to be hidden from user...ideally serer should not be senting.
    let patCondtionsToHide = ["Caronavirus"]
    //collection view referene
    var collectionView:UICollectionView?
    //cell id
    let customCellId = "customCellId"
    //need to handle empty/missing image
    let images : [UIImage] = [#imageLiteral(resourceName: "heart"), #imageLiteral(resourceName: "diabetes"), #imageLiteral(resourceName: "copd"),#imageLiteral(resourceName: "pregnancy")]
    
    
    let nextBtn : UIButton = {
        let btn = UIButton()
        let attributedText = NSAttributedString(string: "Next", attributes: [NSAttributedString.Key.font: UIFont(name: "Bogle-Bold", size: 26)!, NSAttributedString.Key.foregroundColor : UIColor.white])
        btn.setAttributedTitle(attributedText, for: .normal)
        btn.contentMode = .scaleAspectFit
        btn.addTarget(self, action: #selector(nextBtnClicked), for: .touchUpInside)
        btn.translatesAutoresizingMaskIntoConstraints = false
        return btn
    }()
    
    let screenTitle: UILabel = {
        let label = UILabel()
        
        let attributedText = NSAttributedString(string: "Select A Condition", attributes: [NSAttributedString.Key.font: UIFont(name: "Bogle-Bold", size: 26)!, NSAttributedString.Key.foregroundColor : UIColor.white])
        label.attributedText = attributedText
        label.backgroundColor = .clear
        label.textAlignment = .left
        label.numberOfLines = 1
//        label.adjustsFontSizeToFitWidth = true
//        label.minimumScaleFactor = 0.2
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let titleTextView: UILabel = {
        let textview = UILabel()
        
        let attributedText = NSAttributedString(string: "Please select a condition and let us customize your experience", attributes: [NSAttributedString.Key.font: UIFont(name: "Bogle-Bold", size: 20)!, NSAttributedString.Key.foregroundColor : Constants.COMMON_BLUE])
        textview.attributedText = attributedText
//        textview.isEditable = false
//        textview.isSelectable = false
//        textview.textAlignment = .center
        textview.numberOfLines = 0
        textview.minimumScaleFactor = 0.2
        textview.adjustsFontForContentSizeCategory = true
        textview.adjustsFontSizeToFitWidth = true
        textview.translatesAutoresizingMaskIntoConstraints = false
        
        return textview
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //setUp the main view
        setUpMainView()
        //populate patient conditions from server
        //in some cases all the conditions from server need not be displayed in app
        getPatientConditionsfromServer()
      
    }
    

    func setUpMainView()
    {
        let backgroundImage = UIImageView(frame: UIScreen.main.bounds)
        backgroundImage.image = #imageLiteral(resourceName: "healthSessionBgImg")
        self.view.insertSubview(backgroundImage, at: 0)
        
        self.view.addSubview(screenTitle)
        screenTitle.topAnchor.constraint(equalTo: self.view.topAnchor, constant: 25).isActive = true
        screenTitle.heightAnchor.constraint(equalToConstant: 60).isActive = true
        screenTitle.widthAnchor.constraint(equalTo: self.view.widthAnchor, multiplier: 0.7).isActive = true

        screenTitle.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 10).isActive = true
        self.view.addSubview(nextBtn)
        nextBtn.centerYAnchor.constraint(equalTo: screenTitle.centerYAnchor).isActive = true
        nextBtn.widthAnchor.constraint(equalTo: self.view.widthAnchor, multiplier: 0.2).isActive = true
        nextBtn.heightAnchor.constraint(equalToConstant: 60).isActive = true
        nextBtn.trailingAnchor.constraint(equalTo: self.view.trailingAnchor, constant: -10).isActive = true
        
        let bgView = UIView()
        bgView.backgroundColor = .white
        bgView.translatesAutoresizingMaskIntoConstraints = false
        self.view.addSubview(bgView)
        
        bgView.topAnchor.constraint(equalTo: screenTitle.bottomAnchor, constant: 16).isActive = true
        bgView.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 10).isActive = true
        bgView.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -10).isActive = true
        bgView.bottomAnchor.constraint(equalTo: self.view.bottomAnchor, constant: -10).isActive = true
        
        bgView.addSubview(titleTextView)
        
        titleTextView.topAnchor.constraint(equalTo: bgView.topAnchor, constant: 16).isActive = true
        titleTextView.leadingAnchor.constraint(equalTo: bgView.leadingAnchor, constant: 32).isActive = true
        titleTextView.widthAnchor.constraint(equalTo: bgView.widthAnchor, multiplier: 0.9).isActive = true
        titleTextView.heightAnchor.constraint(equalToConstant: 80).isActive = true
        
        let flowLayout = UICollectionViewFlowLayout()
        flowLayout.minimumLineSpacing = 0
        flowLayout.minimumInteritemSpacing = 0
        //collection view
        self.collectionView = UICollectionView(frame: CGRect(x: 0, y: 100, width: self.view.frame.width*0.95, height: self.view.frame.height), collectionViewLayout: flowLayout)
        self.collectionView!.dataSource = self
        self.collectionView!.delegate = self
        bgView.addSubview(self.collectionView!)
        bgView.bringSubviewToFront(self.collectionView!)
        
        self.collectionView!.backgroundColor = .clear
        self.collectionView!.register(CustomCell.self, forCellWithReuseIdentifier: customCellId)
        
        self.collectionView!.isUserInteractionEnabled = true

    }
    
    @objc func nextBtnClicked()
    {
        var selectedConditionCount=0
        
        for conditionItem in patientCondtions{
            if(conditionItem.isSelected){
                RPMUserSettings.sharedInstance.patientCondition.append(conditionItem)
                selectedConditionCount+=1
            }
        }
        if  selectedConditionCount == 0{
            self.showAlert(title: "Info", message: "Please select a condition to proceed")
        }else{
            //save each patient Condition
            
            for condition in RPMUserSettings.sharedInstance.patientCondition{
            NetworkManager().addpatientcondition(condition.iD, completion: {
                _ in
            })
            }
            //login to the dashboard
            logintoapp()
        }
        
    }
    
    func logintoapp()
    {

        
        self.showSpinner(onView: self.view)
        NetworkManager().login(username: RPMUserSettings.sharedInstance.username, password: RPMUserSettings.sharedInstance.password) { (response) -> () in
            self.removeSpinner(completion: {
                if response != nil {
                    self.performSegue(withIdentifier: "AilmentToDashboard", sender: self)
                }
               
            })
    }
    }
    
    var isImgSelected = false
    var selectedAilments = [String]()
    
    @objc func tappedOnImage(gestureRecognizer: UIGestureRecognizer)
    {
        print("selected")
    }
    
    
    func showAlert (title: String, message: String){
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        self.present(alert, animated: true, completion: nil);
    }

    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.patientCondtions.count

    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let customCell = collectionView.dequeueReusableCell(withReuseIdentifier: customCellId, for: indexPath) as! CustomCell
        customCell.backgroundColor = .clear
        //check if a corresponding image is available in local
        if(indexPath.row>=images.count){
            //in this case the image is missing so for now do NOT add an image
        }else{
            customCell.mainImageView.image = images[indexPath.row]
        }
        customCell.mainImageView.image = images[indexPath.row]
        customCell.instructionLabel.text = self.patientCondtions[indexPath.item].name
        customCell.iD = self.patientCondtions[indexPath.row].iD
        return customCell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let padding: CGFloat = 0
        let collectionViewSize = collectionView.frame.size.width - padding
        
        return CGSize(width: collectionViewSize/2.2, height: collectionViewSize/2.2)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0.0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0.0
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath){
        
        let cell = collectionView.cellForItem(at: indexPath) as! CustomCell
        //toggle the selection flag
        patientCondtions[indexPath.row].isSelected.toggle()
        //set selected image
        if(patientCondtions[indexPath.row].isSelected){
            cell.selectionButton.setImage(#imageLiteral(resourceName: "selected-image"), for: .normal)
        }
        else{
            cell.selectionButton.setImage(UIImage(named: "unselected-image"), for: .normal)
        }
    }
    
    func getPatientConditionsfromServer(){
        
        //clear patientCondtions before adding
        patientCondtions.removeAll()
        let rpmParameters: [String: Any] = ["CustomerId":"09349ed1-a358-456b-a6fb-3257df3d7ffb"]
        NetworkManager().getPatientConditions(requestData: rpmParameters){(response) -> () in
            
            if let value = response as? [String: Any]{
                
                let details : [[String:Any]] = value["PageItems"] as! [[String : Any]]
                
                for condition in details{
                    let patientCon : RPMPatientCondition = RPMPatientCondition(patientCondition: condition)
                    //populate patient conditions from server
                    //in some cases all the conditions from server need not be displayed in app
                    //alimesnts to hide are in patCondtionsToHide(only names used...ID can change in server)
                    if(!self.patCondtionsToHide.contains(patientCon.name)){
                            //only add RPMPatientCondition NOT listed in patCondtionsToHide
                            self.patientCondtions.append(patientCon)
                    }
                    
                }
                //update Collection View
                DispatchQueue.main.async {
                    //ideal code but does not work
                    self.collectionView!.reloadData()
                    /**
                    //alternative approach use this code if reloadData faces issues
                    var indexPaths: [NSIndexPath] = []
                    for i in 0..<self.patientCondtions.count {
                        indexPaths.append(NSIndexPath(item: i, section: 0))
                    }
                    self.collectionView?.reloadItems(at: indexPaths as [IndexPath])
                    **/
                }
                
            }
        }
        
    }
    
}


//MARK: - UICollectionViewCell
class CustomCell: UICollectionViewCell {
    var isImgSelected = false
    var iD:String!
    let mainImageView:UIImageView = {
        let imageView = UIImageView()
        imageView.image = UIImage(named: "copd")
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
    }()
    
    let instructionLabel: UILabel = {
        let label = UILabel()
        label.text = "Pregnancy"
        label.textAlignment = .center
        label.textColor = #colorLiteral(red: 0.2030155957, green: 0.6176377535, blue: 0.9852994084, alpha: 1)
        label.font = UIFont(name: "Bogle-Bold", size: 15)
        label.numberOfLines = 0
        label.lineBreakMode = .byWordWrapping
        
        label.backgroundColor = UIColor(red: 1, green: 1, blue: 1, alpha: 0.5)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setUpView()
    }
    
    let selectionButton = UIButton()
   
    
    func setUpView()
    {
        addSubview(mainImageView)
        mainImageView.centerXAnchor.constraint(equalTo: self.contentView.centerXAnchor).isActive = true
        mainImageView.centerYAnchor.constraint(equalTo: self.contentView.centerYAnchor).isActive = true
        mainImageView.widthAnchor.constraint(equalTo: self.contentView.widthAnchor, multiplier: 0.9).isActive = true
        mainImageView.heightAnchor.constraint(equalTo: self.contentView.heightAnchor, multiplier: 0.65).isActive = true
        
        selectionButton.setBackgroundImage(UIImage(named: "unselected-image"), for: .normal)
        selectionButton.translatesAutoresizingMaskIntoConstraints = false
        mainImageView.addSubview(selectionButton)
        selectionButton.topAnchor.constraint(equalTo: mainImageView.topAnchor, constant: 16).isActive =  true
        selectionButton.leadingAnchor.constraint(equalTo: mainImageView.leadingAnchor, constant: 16).isActive = true
        selectionButton.widthAnchor.constraint(equalToConstant: 35).isActive = true
        selectionButton.heightAnchor.constraint(equalToConstant: 35).isActive = true
        selectionButton.addTarget(self, action: #selector(selectImage(sender:)), for: .touchUpInside)
        
        mainImageView.addSubview(instructionLabel)
        instructionLabel.centerYAnchor.constraint(equalTo: mainImageView.centerYAnchor).isActive = true
        instructionLabel.heightAnchor.constraint(equalToConstant: 60).isActive = true
        instructionLabel.leadingAnchor.constraint(equalTo: mainImageView.leadingAnchor).isActive = true
        instructionLabel.widthAnchor.constraint(equalTo: mainImageView.widthAnchor).isActive = true
    }
    
    
    
    @objc func selectImage(sender button: UIButton)
    {
        print("selected")
        
        isImgSelected = !isImgSelected
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    
}
