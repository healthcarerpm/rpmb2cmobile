//
//  EducationViewController.swift
//  Sample
//
//  Created by Himanshu on 8/14/19.
//  Copyright © 2019 Isham. All rights reserved.
//

import UIKit
import WebKit

class EducationViewController: UIViewController {

    let webView = WKWebView()
    override func viewDidLoad() {
        super.viewDidLoad()
        //background
        self.view.addBackgroundImage()
        if let url = URL(string: "http://brainmeshintegrumportal.azurewebsites.net/Education") {
            let request = URLRequest(url: url)
            webView.load(request)
        }
        // Do any additional setup after loading the view.
    }
    
    override func loadView() {
        self.view = webView
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
