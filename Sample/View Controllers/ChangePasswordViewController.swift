//
//  ChangePasswordViewController.swift
//  Sample
//
//  Created by Thejus Jose on 06/04/20.
//  Copyright © 2020 Isham. All rights reserved.
//

import UIKit
import SWRevealViewController

class ChangePasswordViewController: UIViewController {
    
    @IBOutlet weak var sidemenuBar: UIBarButtonItem!
    @IBOutlet weak var oldPasswordTextField: UITextField!
    @IBOutlet weak var newPasswordTextField: UITextField!
    @IBOutlet weak var confirmNewPassword: UITextField!
    @IBOutlet weak var imageTopConstraint: NSLayoutConstraint!
//    @IBOutlet var bannerView: GADBannerView!
    @IBOutlet var submitButton: BasicButton!
//    let backgroundImage = UIImageView(frame: UIScreen.main.bounds)
    
    @IBOutlet weak var dismissBtn: UIBarButtonItem!
    @IBAction func Dismiss(_ sender: UIBarButtonItem) {
        self.dismiss(animated: true, completion: nil)
    }
    //MARK:- Change Passcode
    var keyboardHeight : CGFloat?

    var ENTER_OLD_PASSCODE = "Enter Old Passcode"

    var ENTER_NEW_PASSCODE = "Enter New Passcode"

    var ENTER_CONFIRM_PASSCODE = "Confirm New Passcode"
    
    let ALL_FIELD_MENDATORY =  "Problem: Please fill out all of the required fields."

    let SUBMIT = "SUBMIT"

    //MARK:- Feedback

    let CATEGORY = "Category"
    
    //Change Password
    let CHNAGE_PASSWORD_URL = "ChangePassword"
    
    var parameter = [String:Any]()
    
   var window: UIWindow?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //background
        self.view.addBackgroundImage()
        if RPMUserSettings.sharedInstance.patientId != nil &&  RPMUserSettings.sharedInstance.patientId != ""{
            self.dismissBtn.isEnabled = false
        }else{
             ENTER_OLD_PASSCODE = "New Passcode"

             ENTER_NEW_PASSCODE = "Confirm New Passcode"

             ENTER_CONFIRM_PASSCODE = "OTP from Email"
        }
        
        self.oldPasswordTextField.layer.borderWidth = 1
        self.oldPasswordTextField.layer.cornerRadius = 6
        self.oldPasswordTextField.layer.borderColor = UIColor.gray.cgColor
        self.oldPasswordTextField.setLeftPaddingPoints(6.0)
        self.oldPasswordTextField.doneAccessory = true
        
        self.newPasswordTextField.layer.borderWidth = 1
        self.newPasswordTextField.layer.cornerRadius = 6
        self.newPasswordTextField.layer.borderColor = UIColor.gray.cgColor
        self.newPasswordTextField.setLeftPaddingPoints(6.0)
        self.newPasswordTextField.doneAccessory = true
        
        self.confirmNewPassword.layer.borderWidth = 1
        self.confirmNewPassword.layer.cornerRadius = 6
        self.confirmNewPassword.layer.borderColor = UIColor.gray.cgColor
        self.confirmNewPassword.setLeftPaddingPoints(6.0)
        self.confirmNewPassword.doneAccessory = true
        
        //gesture:tap anywehere to dismiss the keyboard
        let tap = UITapGestureRecognizer(target:self.view,action:#selector(UIView.endEditing))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
        
        let customButton = UIButton(frame: CGRect.init(x: 0, y: 0, width: 20, height: 20))
        customButton.setImage(UIImage(named: "menu"), for: .normal)
        //hide keyboard gesture(tap gesture)
        customButton.addGestureRecognizer(tap)
        customButton.isUserInteractionEnabled = true
        
        if self.revealViewController() != nil {
            customButton.addTarget(self.revealViewController(), action: #selector(SWRevealViewController.revealToggle(_:)), for: .touchUpInside)
        self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        }
        self.navigationItem.leftBarButtonItem?.customView = customButton
        
//        self.navigationItem.leftBarButtonItem = leftBarButton
        
//        addGestureRecognize(tap)
//        self.navigationController?.navigationBar.topItem?.leftBarButtonItem
        
        
       
        self.setPlaceholderOfTextField()
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
         
         NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)

    }
    
    override func viewWillAppear(_ animated: Bool) {

    }
    
    @objc func keyboardWillShow(notification: NSNotification) {
        keyboardHeight = keyBoardHeight()
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            if self.view.frame.origin.y == 0 {
                self.view.frame.origin.y -= keyboardHeight ?? keyboardSize.height/1.3//keyboardSize.height/2
            }
        }
    }

        @objc func keyboardWillHide(notification: NSNotification) {
            if self.view.frame.origin.y != 0 {
                self.view.frame.origin.y = 0
            }
        }

    func keyBoardHeight() -> CGFloat {
        return keyboardHeight ?? 150
    }

    @objc func setPlaceholderOfTextField() {
         self.navigationItem.title = "Change Passcode".localized()
        self.oldPasswordTextField.attributedPlaceholder =  UIUtilFunctions.Instance.setTextPlaceholderAndColor(ENTER_OLD_PASSCODE)
        self.newPasswordTextField.attributedPlaceholder = UIUtilFunctions.Instance.setTextPlaceholderAndColor(ENTER_NEW_PASSCODE)
        self.confirmNewPassword.attributedPlaceholder = UIUtilFunctions.Instance.setTextPlaceholderAndColor(ENTER_CONFIRM_PASSCODE)
        self.submitButton.setTitle(SUBMIT, for: UIControl.State.normal)
        
    }
    //Change Status bar color
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    @IBAction func submitButton_Tapped(_ sender: Any) {
        
        
        if RPMUserSettings.sharedInstance.patientId != nil &&  RPMUserSettings.sharedInstance.patientId != ""{
          if isAllFieldValidation() == false{
              UIUtilFunctions.Instance.AlertMessage(vc: self, ALL_FIELD_MENDATORY, "Alert!")
          }
          else if (confirmNewPassword.text?.count)! < 4 || (newPasswordTextField.text?.count)! < 4{
               UIUtilFunctions.Instance.AlertMessage(vc: self, "Passcode must have 4-digit.", "Alert!")
          }
          else if isOldPasswordEqualNewPassword() == false {
              UIUtilFunctions.Instance.AlertMessage(vc: self, "Problem: Old Passcode and New Passcode should not Match", "Alert!")
          }
          else if isNewPasswordConfirmPasswordMatch() == false{
              UIUtilFunctions.Instance.AlertMessage(vc: self, "Problem: Old Passcode and New Passcode should not Match", "Alert!")
          }
          else
          {
              self.parameter = ["email":RPMUserSettings.sharedInstance.username,"OldPassword":self.oldPasswordTextField.text!,"Password":self.newPasswordTextField.text!,"UserName":RPMUserSettings.sharedInstance.username,"ConfirmPassword": confirmNewPassword.text!,"Code":"string"]
              
              NetworkManager().changepassword(params: self.parameter, completion: {
                  (changes:String) in
                  if changes == "Success"{
                   let alert = UIAlertController(title: "Change Passcode", message: "Passcode updated.", preferredStyle: .alert)
                  alert.addAction(UIAlertAction(title: "Ok",style:.cancel,handler: nil))
                  self.present(alert, animated: true, completion: nil)
                  self.oldPasswordTextField.text = ""
                  self.newPasswordTextField.text = ""
                  self.confirmNewPassword.text = ""
                  }else{
                       let alert = UIAlertController(title: "Error", message: "Passcode could not be changed. Please verify the old passcode and try again", preferredStyle: .alert)
                      alert.addAction(UIAlertAction(title: "Ok",style:.cancel,handler: nil))
                      self.present(alert, animated: true, completion: nil)
                      self.oldPasswordTextField.text = ""
                      self.newPasswordTextField.text = ""

                  }
              })

          
          }
        }
        else{
            if isAllFieldValidation() == false{
                UIUtilFunctions.Instance.AlertMessage(vc: self, ALL_FIELD_MENDATORY, "Alert!")
            }
            else if (confirmNewPassword.text?.count)! < 4 || (newPasswordTextField.text?.count)! < 4{
                UIUtilFunctions.Instance.AlertMessage(vc: self, "Passcode must have 4-digit.", "Alert!")
            }
            else if isOldPasswordNotEqualNewPassword() == false {
                UIUtilFunctions.Instance.AlertMessage(vc: self, "Problem: Old Passcode and New Passcode should not Match", "Alert!")
            }
            else
            {
                var username = ""
                if UserDefaults.standard.value(forKey: "username") != nil{
                       username  = UserDefaults.standard.value(forKey: "username") as! String
                       }
                
                self.parameter = ["email": RPMUserSettings.sharedInstance.username ,"OldPassword":"","Password":self.oldPasswordTextField.text!,"UserName": RPMUserSettings.sharedInstance.username ,"ConfirmPassword": newPasswordTextField.text!,"Code":self.confirmNewPassword.text!]
                
                NetworkManager().changepasswordwithoutlogin(params: self.parameter, completion: {
                    (changes:String) in
                    if changes == "Success"{
                        let alert = UIAlertController(title: "Change Passcode", message: "Passcode updated.", preferredStyle: .alert)
                        alert.addAction(UIAlertAction(title: "Ok",style:.cancel,handler: nil))
                        self.present(alert, animated: true, completion: nil)
                        self.oldPasswordTextField.text = ""
                        self.newPasswordTextField.text = ""
                        self.confirmNewPassword.text = ""
                    }else{
                        let alert = UIAlertController(title: "Error", message: "Passcode could not be changed.", preferredStyle: .alert)
                        alert.addAction(UIAlertAction(title: "Ok",style:.cancel,handler: nil))
                        self.present(alert, animated: true, completion: nil)
                        self.oldPasswordTextField.text = ""
                        self.newPasswordTextField.text = ""
                        
                    }
                })
                
                
            }
        }
    }
    
    func isAllFieldValidation() -> Bool {
        if (self.oldPasswordTextField.text?.isEmpty)! || (self.newPasswordTextField.text?.isEmpty)! || (self.confirmNewPassword.text?.isEmpty)! {
            return false
        }
        else{
            return true
        }
    }
    
    func isOldPasswordEqualNewPassword() -> Bool {
        if self.oldPasswordTextField.text! == self.newPasswordTextField.text!{
            return false
        }
        else{
            return true
        }
    }
    
    func isOldPasswordNotEqualNewPassword() -> Bool {
        if self.oldPasswordTextField.text! != self.newPasswordTextField.text!{
            return false
        }
        else{
            return true
        }
    }

    
    func isNewPasswordConfirmPasswordMatch() -> Bool {
       if self.newPasswordTextField.text! == self.confirmNewPassword.text!{
        return true
    }
    else{
        return false
      }
    }
}

extension ChangePasswordViewController:UITextFieldDelegate{
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if range.location > 3{
            return false
        }
        
        let nsString:NSString? = textField.text as NSString?
        let updatedString = nsString?.replacingCharacters(in:range, with:string);
        
        textField.text = updatedString
        //Setting the cursor at the right place
        let selectedRange = NSMakeRange(range.location + string.count, 0)
        let from = textField.position(from: textField.beginningOfDocument, offset:selectedRange.location)
        let to = textField.position(from: from!, offset:selectedRange.length)
        textField.selectedTextRange = textField.textRange(from: from!, to: to!)
        
        //Sending an action
        textField.sendActions(for: UIControl.Event.editingChanged)
        
        return false
    }
    
//    // MARK: - Search Method
//    func textFieldShouldReturn(_ textField: UITextField) -> Bool
//    {
//
//        textField.resignFirstResponder()
//        return true
//    }
}

