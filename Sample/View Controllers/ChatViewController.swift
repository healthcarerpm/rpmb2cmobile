//
//  ChatViewController.swift
//  Custom-Chat
//
//  Created by Bertuğ YILMAZ on 19/07/2017.
//  Copyright © 2017 bertug. All rights reserved.
//

import UIKit

class ChatViewController: UIViewController {
    
    @IBOutlet weak var bottomView: UIView!     // View was used instead of toolbar because it is more useful :]
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var bottomViewContraits: NSLayoutConstraint!
    @IBOutlet weak var messageTextView: UITextView!
    
    @IBOutlet weak var sendButton: UIButton!
    var thatsIam : Bool = true
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //background
    //    self.view.addBackgroundImage()
        self.navigationController?.navigationBar.isTranslucent = false
        UserDefaults.standard.set(true, forKey: "seen")
        MeasurementIdentifier.sharedInteractor.initialmessagecounter = (MeasurementIdentifier.sharedInteractor.messages?.count)!
        UserDefaults.standard.synchronize()
        self.textViewSettings()
        self.tableViewSettings()
        self.keyboardSettings()
        if #available(iOS 13.0, *) {
                   overrideUserInterfaceStyle = .light
               } else {
                   // Fallback on earlier versions
               }
        messageTextView.doneAccessory = true
    }
    
    @IBAction func sendmessage(_ sender: Any) {
        print(self.messageTextView.text)
        
        if messageTextView.text == ""
        {
            
        }
        else{
            NetworkManager().postpatientMessage(MeasurementIdentifier.sharedInteractor.patientId!, message: self.messageTextView.text)
            //let patientDetails = CoreDataUtils().getUserDetails();
            let message = Message.init(fromId: CoreDataUtils().getUserDetails().patientId!, toID:"0", value: self.messageTextView.text, creationDate: getCurrentDateTimeasString(), frompatient: true, messageId: "12", patientId: CoreDataUtils().getUserDetails().patientId!)
            MeasurementIdentifier.sharedInteractor.messages!.append(message)
            self.messageTextView.text = ""
            self.tableView.reloadData()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.showSpinner(onView: self.view)
        NetworkManager().getpatientmessages(patientId: MeasurementIdentifier.sharedInteractor.patientId!, completion: {
            _ in
            self.removeSpinner(completion: {
                if MeasurementIdentifier.sharedInteractor.messages!.count > 0{
                    //                MeasurementIdentifier.sharedInteractor.messages = MeasurementIdentifier.sharedInteractor.messages?.sorted{ (a,b) -> Bool in
                    //                    return a.creationDate! < b.creationDate!
                    //
                    
                    self.tableView.reloadData()
                    
                }
                else
                {
                    self.tableView.reloadData()
                    //    self.removeSpinner()
                }
            })
            

        })
    }

    func getCurrentDateTimeasString() -> String{
        let currentDateTime = Date()
        let dateTimeFormatter = DateFormatter()
        dateTimeFormatter.timeZone = TimeZone(identifier: "UTC")
        dateTimeFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss'Z'"

//        dateTimeFormatter.dateFormat="yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
        let str:String  = dateTimeFormatter.string(from: currentDateTime)
        print("DATE-TIME \(str)")
        
        return str
    }

    
    //Settings
    
    func keyboardSettings(){
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardNotification(notification:)), name: UIResponder.keyboardWillChangeFrameNotification, object: nil)

        self.view.addGestureRecognizer(UITapGestureRecognizer(target: self.view, action: #selector(UIView.endEditing(_:))))
        let swipeDown = UISwipeGestureRecognizer(target: self.view , action : #selector(UIView.endEditing(_:)))
        swipeDown.direction = .down
        self.view.addGestureRecognizer(swipeDown)
    }
    
    func textViewSettings (){
        self.messageTextView.layer.borderWidth = CGFloat(0.5)
        self.messageTextView.layer.borderColor = UIColor.darkGray.cgColor
        self.messageTextView.layer.cornerRadius = CGFloat(13)
        self.sendButton.layer.borderWidth = CGFloat(0.5)
//        self.messageTextView.layer.borderColor = UIColor.darkGray.cgColor
        self.sendButton.layer.cornerRadius = CGFloat(13)
    }
    
    func tableViewSettings(){
        self.tableView.rowHeight = UITableView.automaticDimension
        self.tableView.estimatedRowHeight = 140
        self.tableView.delegate = self
        self.tableView.dataSource = self
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    //Keyboard Configure
    
    @objc func keyboardNotification(notification: NSNotification) {
                if let userInfo = notification.userInfo {
                    if let endFrame = (userInfo[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue{
                        if let duration:TimeInterval = (userInfo[UIResponder.keyboardAnimationDurationUserInfoKey] as? NSNumber)?.doubleValue{
        
                            let animationCurveRawNSN = userInfo[UIResponder.keyboardAnimationCurveUserInfoKey] as? NSNumber
                            let animationCurveRaw = animationCurveRawNSN?.uintValue ?? UIView.AnimationOptions.curveEaseInOut.rawValue
                            let animationCurve:UIView.AnimationOptions = UIView.AnimationOptions(rawValue: animationCurveRaw)
        
                            if (endFrame.origin.y) >= UIScreen.main.bounds.size.height {
                                self.bottomViewContraits?.constant = 0.0
                            } else {
                                self.bottomViewContraits?.constant = endFrame.size.height
                            }
        
                            UIView.animate(withDuration: duration,
                                           delay: TimeInterval(0),
                                           options: animationCurve,
                                           animations: { self.view.layoutIfNeeded() },
                                           completion: nil)
                        }
                    }
                }
    }
    
    
}
extension ChatViewController : UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        if MeasurementIdentifier.sharedInteractor.messages != nil  {
            return MeasurementIdentifier.sharedInteractor.messages!.count
        }
        else{
            return 0
        }
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{

        let message = MeasurementIdentifier.sharedInteractor.messages![indexPath.row]
     
        if message.fromId == CoreDataUtils().getUserDetails().patientId {
            let cell = tableView.dequeueReusableCell(withIdentifier: "senderCell") as! SenderTableViewCell
//            cell.senderImageView.layer.cornerRadius = 15
            cell.senderMessageTextView.layer.cornerRadius = 8
//            cell.senderImageView.image = UIImage(named: "user")
            if message.value != nil{
            cell.senderMessageTextView.text = message.value!
            }
            cell.lastTimeLabel.text =  getDateascontinuesStrings(datefromfunction: message.creationDate!)
            return cell
            
        }
        else
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "receiverCell", for: indexPath) as! ReceiverTableViewCell
            cell.receiverMessageTextView.layer.cornerRadius = 8
            cell.receiverMessageTextView.text = message.value!
//            cell.lastTimeLabel.text = message.creationDate.formatt
            cell.lastTimeLabel.text = getDateascontinuesStrings(datefromfunction: message.creationDate!)
            cell.senderName.text = message.fromUserName
            return cell

        }
   }
    
    func getDateascontinuesStrings(datefromfunction:String) -> (String)
    {
        let dateFormatter = DateFormatter()
//        var date:Date? = nil
        dateFormatter.locale = Locale.current
        
        // save locale temporarily
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
        var date = dateFormatter.date(from:datefromfunction)
        if date == nil
        {
            //dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SS'Z'
            date = dateFormatter.date(from:datefromfunction)
        }
        /*
         Temporaray fix to handle both mobile and server values
         */
//        if date == nil{
//            dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
//
//            date = dateFormatter.date(from:datefromfunction)
//
//        }
        
        
        let dateFormatterWithDay = DateFormatter()
        dateFormatterWithDay.dateStyle = .medium
        dateFormatterWithDay.timeStyle = .medium
//        dateFormatterWithDay.dateFormat = "EEEE MMM d, yyyy HH:mm"
        if let value = date {
            let dateString = dateFormatterWithDay.string(from: value)
            return dateString
        }
        return ""
    }

    
}

extension Date {
   struct Formatter {
       static let utcFormatter: DateFormatter = {
           let dateFormatter = DateFormatter()
 
           dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss'Z'"
           dateFormatter.timeZone = TimeZone(identifier: "GMT")
 
           return dateFormatter
       }()
   }
 
   var dateToUTC: String {
       return Formatter.utcFormatter.string(from: self)
   }
}
 
extension String {
   struct Formatter {
       static let utcFormatter: DateFormatter = {
           let dateFormatter = DateFormatter()
           dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ssz"
           
           return dateFormatter
       }()
   }
 
   var dateFromUTC: Date? {
       return Formatter.utcFormatter.date(from: self)
   }
}
