//
//  NoninInputViewController.m
//
//  Created by Keith Thomas on 4/7/16.
//  Copyright © 2016 OdysseyInc, LLC. All rights reserved.
//
#import "NoninInputViewController.h"
#import <CoreBluetooth/CoreBluetooth.h>


#define kServiceUUID  @"46A970E0-0D5F-11E2-8B5E-0002A5D5C51B"
#define kPeripheralUUID @"DCB8D975-1314-5572-2EDF-CD7CD80EB52D"
#define kCharacteristicsUUID @"0AAD7EA0-0D60-11E2-8E3C-0002A5D5C51B"
#define kControlPointCharacteristicUUID @"1447AF80-0D60-11E2-88B6-0002A5D5C51B"

@interface NoninInputViewController() <CBCentralManagerDelegate, CBPeripheralDelegate>

@property(nonatomic, strong) CBCentralManager *centralManager;
@property(nonatomic,strong) CBPeripheral *discoveredPeripheral;

@end

@implementation NoninInputViewController
{
    BOOL measurementComplete;
    CBCharacteristic * controlPointCharacteristic;
    NSTimer *deviceNotFoundTimer;
}

/*
 =======================================================================================
 Function: viewDidLoad
 Description: Called after the controller's view is loaded into memory
 Parameters: None
 Returns: None
 =======================================================================================
 */
- (void)viewDidLoad
{
    [super viewDidLoad];
    //Initialize the class and set the delegate
    self.centralManager = [[CBCentralManager alloc] initWithDelegate:self queue:nil];
    //self.isHealthSession = NO;
    //measurementComplete = NO;
    [self setupButtons];
    [self setScreenState:ProcessStateNPOInstruction withSat:0 withPulse:0];
    //[self setupToolbar];
    
    [self startTimer];
}

/*
 =======================================================================================
 Function: centralManagerDidUpdateState
 Description: Invoked when central manager (i.e. the iOS device) state is updated
 Parameters: central
 Returns: None
 =======================================================================================
 */
- (void) centralManagerDidUpdateState:(CBCentralManager *) central
{
    switch(central.state)
    {
            //BLE is powered on and ready
        case CBManagerStatePoweredOn:
            //Start the scanning for any devices
            [self scan];
            break;
            
            //BLE is powered off
            //Note: Here, you can re-initialize the central Manager so that the iOS device will alert the user to turn on the Bluetooth
        case CBManagerStatePoweredOff:
            NSLog(@"PoweredOff!");
            self.discoveredPeripheral = nil;
            
            [self setScreenState:ProcessStateNPOErrorNoBluetooth withSat:0 withPulse:0];
            
            break;
        default:
            break;
    }
}


/*
 =======================================================================================
 Function: scan
 Description: scans for peripherals that are advertising services (any BLE device)
 Parameters: None
 Returns: None
 =======================================================================================
 */
-(void) scan
{
    NSLog(@"Start Scan");
    CBUUID *spo2ServiceUUID = [CBUUID UUIDWithString:kServiceUUID];
    [self.centralManager scanForPeripheralsWithServices:@[spo2ServiceUUID] options:@{CBCentralManagerScanOptionAllowDuplicatesKey:@YES}];
}


/*
 =======================================================================================
 Function: didDicoverPeripheral
 Description: Invoked when the iOS device discovers a peripheral while scanning
 Parameters: central, peripheral, adertisementData, RSSI
 Returns: None
 =======================================================================================
 */
-(void)centralManager:(CBCentralManager *) central didDiscoverPeripheral:(CBPeripheral *)peripheral advertisementData:(NSDictionary *)advertisementData RSSI:(NSNumber *)RSSI
{
    if(self.discoveredPeripheral != peripheral)
    {
        self.discoveredPeripheral = peripheral;
        NSLog(@"Peripheral Discovered: %@", peripheral.name);
        //Try connecting to the device whose name has the prefix Nonin3230
        if([peripheral.name hasPrefix:@"Nonin3230"])
        {
            //Stop scanning
            [self.centralManager stopScan];
            
            //self.statusLabel.text = @"Connecting to device.";
            [self setScreenState:ProcessStateNPOConnecting withSat:0 withPulse:0];
            
            NSLog(@"Scanning Stopped!");
            [self.centralManager connectPeripheral:peripheral options:nil];
        }
    }
}


/*
 =======================================================================================
 Function: didConnectperipheral
 Description: Invoked when a connection is sucessfully created with a peripheral
 Parameters: central, peripheral
 Returns: None
 =======================================================================================
 */
-(void) centralManager:(CBCentralManager *) central didConnectPeripheral:(CBPeripheral *)peripheral
{
    // Asks the peripheral to discover all services
    peripheral.delegate = self;
    [peripheral discoverServices:nil];
    
    //self.statusLabel.text = @"Connected. Waiting for measurement.";
    [self setScreenState:ProcessStateNPOMeasuring withSat:0 withPulse:0];
    
    NSLog(@"Connection sucessfull to peripheral: %@ with UUID: %@", peripheral, peripheral.identifier);
}


/*
 =======================================================================================
 Function: didFailToConnectperipheral
 Description: Invoked when the iOS device fails to create a connection with a peripheral
 Parameters: central, peripheral, error
 Returns: None
 =======================================================================================
 */
-(void) centralManager:(CBCentralManager *) central didFailToConnectPeripheral:(CBPeripheral *)peripheral error:(NSError *) error
{
    NSLog(@"Connection Failed");
    //Do something when a connection to a peripheral fails
    self.discoveredPeripheral = nil;
    [self scan];
}


/*
 =======================================================================================
 Function: didDiscoverServices
 Description: Invoked when you discover the peripheral's available services
 Parameters: peripheral, error
 Returns: None
 =======================================================================================
 */
- (void)peripheral:(CBPeripheral *) peripheral didDiscoverServices:(NSError *)error
{
    if(error)
    {
        NSLog(@"Error dicovering service: %@", [error localizedDescription]);
        [self cleanUp];
        return;
    }
    
    for (CBService *service in peripheral.services)
    {
        // Discovers the characteristics for a given service
        if ([service.UUID isEqual:[CBUUID UUIDWithString:kServiceUUID]])
        {
            [peripheral discoverCharacteristics:nil forService:service];
        }
    }
}


/*
 =======================================================================================
 Function: didDiscoverCharacteristicsForService
 Description: Invoked when you discover the characteristics of a specified service
 Parameters: peripheral, service, error
 Returns: None
 =======================================================================================
 */
- (void)peripheral:(CBPeripheral *)peripheral didDiscoverCharacteristicsForService:(CBService *)service error:(NSError *)error
{
    if(error)
    {
        NSLog(@"Error discovering characteristics: %@", [error localizedDescription]);
        [self cleanUp];
        return;
    }
    
    for (CBCharacteristic *characteristic in service.characteristics)
    {
        if ([characteristic.UUID isEqual:[CBUUID UUIDWithString:kCharacteristicsUUID]])
        {
            //Required device found, turn on the notifications
            NSLog(@"Measurement: %@", characteristic.UUID);
            [peripheral setNotifyValue:YES forCharacteristic:characteristic];
            
        }
        else if ([characteristic.UUID isEqual:[CBUUID UUIDWithString:kControlPointCharacteristicUUID]])
        {
            //Required device found, turn on the notifications
            NSLog(@"Control Point: %@", characteristic.UUID);
            [peripheral setNotifyValue:YES forCharacteristic:characteristic];
            controlPointCharacteristic = characteristic;
            
            unsigned char b[2] = {0x61, 0x05};
            
            NSData * d = [NSData dataWithBytes:b length:2];
            [self.discoveredPeripheral writeValue:d forCharacteristic:controlPointCharacteristic type:CBCharacteristicWriteWithResponse];
            
        }
    }
}


/*
 =======================================================================================
 Function: didUpdateValueForCharacteristics
 Description: Invoked when you retrieve a specifed characteristics's value
 Parameters: peripheral, characteristic, error
 Returns: None
 =======================================================================================
 */
- (void)peripheral:(CBPeripheral *)peripheral didUpdateValueForCharacteristic:(CBCharacteristic *)characteristic error:(NSError *)error
{
    if(error)
    {
        NSLog(@"Error on characteristic update: %@", [error localizedDescription]);
        return;
    }
    
    if ([characteristic.UUID isEqual:[CBUUID UUIDWithString:kCharacteristicsUUID]])
    {
        //Parse the received data
        [self parseData:characteristic.value];
    }
    else if ([characteristic.UUID isEqual:[CBUUID UUIDWithString:kControlPointCharacteristicUUID]])
    {
        [self parseControlPointData:characteristic.value];
        
    }
    
}

- (void) parseControlPointData:( NSData *) data{
    
    const uint8_t *bytes = [data bytes];
    
    uint8_t response    = bytes[0];
    uint8_t status      = bytes[1];
    
    NSLog(@"response: %d", response);
    NSLog(@"status: %d", status);
    
    
}

/*
 =======================================================================================
 Function: parseData
 Description: The receieved data are parsed here into measurements
 Parameters: data
 Returns: None
 =======================================================================================
 */

- (void)parseData:(NSData *)data
{
    const uint8_t *bytes = [data bytes];
    //Indicates the current device status
    uint8_t status    = bytes[1];
    /*----------------Status field----------------*/
    //Indicates that the display is synchronized to the SpO2 and pulse rate values contained in this packet
    uint8_t syncIndication = (status & 0x1);
    //Average amplitude indicates low or marginal signal quality
    //Used to indicate tht the data successfully passed the SmartPoint Algorithm
    uint8_t smartPoint  = (status & 0x4) >> 2;
    //An absence of consecutive good pulse signal
    uint8_t searching   = (status & 0x8) >> 3;
    //CorrectCheck technology indicates that the finger is placed correctly in the oximeter
    uint8_t correctCheck  = (status & 0x10) >> 4;
    //Low or critical battery is indicated on the device
    uint8_t lowBattery  = (status & 0x20) >> 5;
    //SpO2 percentage 0-100 (127 indicates missing)
    uint8_t spO2      = bytes[7];
    //Pulse Rate in beats per minute, 0-325. (511 indicates missiing)
    uint16_t pulse    = (bytes[8] << 8) | bytes[9];

    if( measurementComplete == NO )
    {
        if( correctCheck == 0 )
        {
            [self setScreenState:ProcessStateNPOErrorFingerNotInserted withSat:0 withPulse:0];
        }
        else if( searching == 1 || ((int)spO2 == 127) || ((int)pulse == 511) )
        {
            [self setScreenState:ProcessStateNPOMeasuring withSat:0 withPulse:0];
        }
        //else if( syncIndication == YES){
        else if( syncIndication && smartPoint )
        {
            [self setScreenState:ProcessStateNPOMeasurementComplete withSat:spO2 withPulse:pulse];
            measurementComplete = YES;
            [self.doneButton setHidden:NO];
            [self.cancelButton setEnabled:NO];
            
            unsigned char b[4] = {0x62, 0x4E, 0x4D, 0x49};
            
            NSData * d = [NSData dataWithBytes:b length:4];
            [self.discoveredPeripheral writeValue:d forCharacteristic:controlPointCharacteristic type:CBCharacteristicWriteWithResponse];
            
        } else if( lowBattery == 1 ) {
            [self setScreenState:ProcessStateNPOErrorLowBattery withSat:0 withPulse:0];
        }
        
        
    }
}


/*
 =======================================================================================
 Function: didDisconnectPeripheral
 Description: Invoked when an existing connection with a peripheral is disconnected
 Parameters: peripheral, error
 Returns: None
 =======================================================================================
 */
-(void) centralManager:(CBCentralManager *) central didDisconnectPeripheral:(CBPeripheral *)peripheral error:(NSError *)error
{
    NSLog(@"Peripheral Disconnected. Error: %@",error);
    //set all the values to nil
    self.discoveredPeripheral = nil;
    //scan
    if( measurementComplete == NO ) {
        [self scan];
    } else {
        [self setScreenState:ProcessStateNPOMeasurementCompleteAndDisconnected withSat:0 withPulse:0];
    }
}


/*
 =======================================================================================
 Function: cleanUp
 Description: Call this when things either go wrong or you're done with the connection
 Parameters: None
 Returns: None
 =======================================================================================
 */
-(void) cleanUp
{
    if(self.discoveredPeripheral.state != CBPeripheralStateConnected)
        return;
    
    if(self.discoveredPeripheral.services != nil)
    {
        for(CBService *service in self.discoveredPeripheral.services)
        {
            if(service.characteristics != nil)
            {
                for(CBCharacteristic *characteristics in service.characteristics)
                {
                    if([characteristics.UUID isEqual:[CBUUID UUIDWithString:kServiceUUID]])
                    {
                        if(characteristics.isNotifying)
                        {
                            [self.discoveredPeripheral setNotifyValue:NO forCharacteristic: characteristics];
                        }
                    }
                }
            }
        }
    }
    
    [self.centralManager cancelPeripheralConnection:self.discoveredPeripheral];
}


- (IBAction)donePressed:(id)sender {
    
    
     if( self.discoveredPeripheral.state == CBPeripheralStateConnected)
        [self.centralManager cancelPeripheralConnection:self.discoveredPeripheral];
        [self.centralManager stopScan];
    
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 0.5 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
              if (self.callingController) {
                [self.navigationController popToViewController:self.callingController animated:YES];
            } else {
                [self.navigationController popToRootViewControllerAnimated:YES];
            }
        });
    }


#pragma mark layout

- (void)setScreenState:(ProcessStateNoninPulseOxType)state withSat:(int)sat withPulse:(int)pulse;
{
    Boolean hideResults = YES;
    Boolean showErrorText = NO;
    
    switch (state) {
        case ProcessStateNPOInstruction:
            self.statusLabel.text = NSLocalizedString( @"Put the Pulse Oximeter on your finger.", "");
            [self.handImage setImage:[UIImage imageNamed:@"arm_startUp.png"]];
            break;
            
        case ProcessStateNPOConnecting:
            self.statusLabel.text = NSLocalizedString(@"Connecting to Pulse Oximeter...", "");
           // self.handImage.backgroundColor = CIColor.clearColor;
            [self.handImage setImage:[UIImage imageNamed:@"arm_startUp.png"]];
            break;
            
        case ProcessStateNPOMeasuring:
            self.statusLabel.text = NSLocalizedString(@"Measuring... Please Wait!!!", "");
            [self.handImage setImage:[UIImage imageNamed:@"arm_startUp.png"]];
            break;
            
        case ProcessStateNPOMeasurementComplete:
            [self clearTimer];
            
            self.statusLabel.text = NSLocalizedString(@"Measurement Complete!", "");
            self.satLabel.text =[NSString stringWithFormat:@"%d",sat];
            self.pulseLabel.text =[NSString stringWithFormat:@"%d",pulse];
            
            self.satTitleLabel.text = NSLocalizedString(@"Oxygen%", "");
            self.satUnitLabel.text = NSLocalizedString(@"%", "");
            self.pulseTitleLabel.text = NSLocalizedString(@"Heart Rate", "");
            self.pulseUnitLabel.text = NSLocalizedString(@"bpm", "");
            [self.handImage setImage:[UIImage imageNamed:@"pulse_Ox.png"]];
            [self.delegate noninInputController:self didCompleteWithMeasurement:@{@"pulse":[NSNumber numberWithInt:pulse],@"spo2":[NSNumber numberWithInt:sat]}];
            hideResults = NO;
            
            break;
            
        case ProcessStateNPOErrorNoBluetooth:
            self.statusLabel.text = NSLocalizedString(@"Bluetooth is powered off.", "");
            [self.handImage setImage:[UIImage imageNamed:@"arm_startUp.png"]];
            break;
            
        case ProcessStateNPOErrorFingerNotInserted:
            self.statusLabel.text = NSLocalizedString(@"Insert your finger all the way.", "");
            [self.handImage setImage:[UIImage imageNamed:@"insert_Fingerpng.png"]];
            break;
            
        case ProcessStateNPOMeasurementCompleteAndDisconnected:
            self.statusLabel.text = NSLocalizedString(@"Remove the device and press Done to continue.", "");
            hideResults = NO;
            [self.handImage setImage:[UIImage imageNamed:@"arm_startUp.png"]];
            break;
            
        case ProcessStateNPOErrorLowBattery:
            self.statusLabel.text = NSLocalizedString(@"Batteries in the device are low and need to be replaced.", "");
            [self.handImage setImage:[UIImage imageNamed:@"arm_startUp.png"]];
            break;
            
        case ProcessStateNPOErrorDeviceNotDetectedTimeout:
            showErrorText = YES;
            self.statusLabel.text = NSLocalizedString(@"The Nonin 3230 Pulse Oximiter device is not detected. Make sure that...", "");
            self.statusLabel.font = [UIFont fontWithName:@"Helvetica Neue" size:40];
            [self.handImage setImage:[UIImage imageNamed:@"arm_startUp.png"]];
            break;
            
        default:
            break;
    }
    
    [self.errorTimeoutExplanation setHidden:!showErrorText];
    
    [self.satUnderline setHidden:hideResults];
    [self.pulseUnderline setHidden:hideResults];
    
    if (hideResults) {
        self.satLabel.text = @"";       // values
        self.pulseLabel.text = @"";
        
        self.satTitleLabel.text = @"";
        self.satUnitLabel.text = @"";
        self.pulseTitleLabel.text = @"";
        self.pulseUnitLabel.text = @"";
    }
    
}


- (void) setupButtons;
{
    
    [self.doneButton setHidden:YES];
    [self.cancelButton setEnabled:YES];
    [self.backButton setEnabled:YES];
    
    [self.backButton setTitle:[NSString stringWithFormat:NSLocalizedString(@"<< Back", "")] forState:UIControlStateNormal];
    [self.doneButton setTitle:[NSString stringWithFormat:NSLocalizedString(@"Done", "")] forState:UIControlStateNormal];
    
    self.backButton.layer.borderColor = [[UIColor whiteColor] CGColor];
    self.backButton.layer.borderWidth = 1;
    self.backButton.backgroundColor = [UIColor clearColor];
    self.backButton.tintColor = [UIColor whiteColor];
    self.backButton.layer.masksToBounds = false;
    self.backButton.layer.cornerRadius = 10;
    self.backButton.titleLabel.font = [UIFont systemFontOfSize:25];
    
    self.doneButton.layer.borderColor = [[UIColor whiteColor] CGColor];
    self.doneButton.layer.borderWidth = 1;
    self.doneButton.backgroundColor = [UIColor clearColor];
    self.doneButton.tintColor = [UIColor whiteColor];
    self.doneButton.layer.masksToBounds = false;
    self.doneButton.layer.cornerRadius = 10;
    self.doneButton.titleLabel.font = [UIFont systemFontOfSize:25];
}



#pragma mark timer

- (void) startTimer;
{
    [self clearTimer];
    deviceNotFoundTimer = [NSTimer scheduledTimerWithTimeInterval:45 target:self selector:@selector(timeExpired) userInfo:nil repeats:NO];
}

- (void) timeExpired;
{
    [self setScreenState:ProcessStateNPOErrorDeviceNotDetectedTimeout withSat:0 withPulse:0];
    [self clearTimer];
}

- (void) clearTimer;
{
    if (deviceNotFoundTimer != nil) {
        [deviceNotFoundTimer invalidate];
    }
    deviceNotFoundTimer = nil;
}


@end
