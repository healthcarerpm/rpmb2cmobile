//
//  HealthSessionViewModel.swift
//  Sample
//
//  Created by Thejus Jose on 12/03/19.
//  Copyright © 2019 Isham. All rights reserved.
//

import Foundation

extension HealthSessionViewController {
    
    class ViewModel {
        private let assessmentId : String
        private let sessionId : String
        private let currentNode : Node
        private let currentParentId : String
        private let sessionName:String!
        private let completedSessionsNumber:Int
        private let totalSessionsNumber:Int
        
        var currentAssessmentId : String {
            get {
                return assessmentId
            }
        }
        
        var currentNodeId: String {
            get {
                return currentNode.id
            }
        }
        
        var sessionnamestring:String {
            get {
                return sessionName
            }
        }
        
        var text:String? {
            get {
                return currentNode.text
            }
        }
        
        var measuremenType:String? {
            get {
                guard currentNode.measurementTypeId != nil else{
                    return ""
                }
                let measurementTypeValue:MeasurementType = CoreDataUtils().getMeasurementType(measurementTypeId: currentNode.measurementTypeId!)
                return measurementTypeValue.name
            }
        }
        let nodeTypeOptions: [String] = [Node.NodeType.alert.rawValue,
                                         Node.NodeType.radio.rawValue,
                                         Node.NodeType.measurement.rawValue,
                                         Node.NodeType.option.rawValue,
                                         Node.NodeType.check.rawValue]
        
        var currentNodeType:String {
            get {
                return currentNode.nodeType.rawValue
            }
         }
        
        var nodeOptions :[Node]? {
            get{
                var nodeList = [Node]()
                for nodeId in currentNode.options ?? [String]() {
                    nodeList.append(CoreDataUtils().getNode(id: nodeId))
                }
                return nodeList
            }
        }
        
        var completedSessionsNo:Float {
            get {
                return Float(completedSessionsNumber)
            }
        }
        var totalSessionsNo:Float {
            get {
                return Float(totalSessionsNumber)
            }
        }
        let alertTypeOptions: [String] = [Node.AlertType.colorless.rawValue,Node.AlertType.green.rawValue, Node.AlertType.red.rawValue]
        init(assessmentId:String,sessionId:String,currentParentId:String,currentNode:Node,sessionName:String,completedSessionsNumber:Int,totalSessionsNumber:Int) {
            self.assessmentId = assessmentId
            self.sessionId = sessionId
            //            self.nodes = nodes
            self.currentParentId = currentParentId
            self.currentNode = currentNode
            self.sessionName = sessionName
            self.completedSessionsNumber = completedSessionsNumber
            self.totalSessionsNumber = totalSessionsNumber
        }
        
        /**
         Save/Update the value and answerId of a Node
         - Parameters:
         - valueGiven : the value to be saved
         - answerIdGiven : answerId to be saved
         
         - Throws: .
         
         - Returns: ViewModel.
         */
        func saveNodeValueAnswer(valueGiven:String, answerIdGiven :String?){
            
            switch(self.currentNode.nodeType.rawValue){
            case "Radio","radio","Check","check":
                
                //update the values of current node-check/radio node
                let coreDataUtils = CoreDataUtils()
                coreDataUtils.updateNode(updatedNodeId:self.currentNode.id,sessionId: self.currentNode.sessionId, updatedValue: valueGiven, answerId: answerIdGiven, measurementDateTime: Date(), isTaken: true,isCompleted:true)
                //untaken sibsling must NOT be updated for root elements i.e : parentId = assessmentId
                if( self.assessmentId != self.currentNode.id ){
                    let untakenSiblings = coreDataUtils.getUntakenChildNodesOfAssessmentParent(assessmentId: self.assessmentId, sessionId:self.sessionId ,parentId: self.currentNode.id)
                    for sibling in untakenSiblings{
                        if(sibling.id == answerIdGiven){
                            coreDataUtils.updateNodeTakenCompleted(updatedNodeId:sibling.id, sessionId: sibling.sessionId, isTaken: true,isCompleted:true)
                        }else{
                            coreDataUtils.updateNodeTaken(updatedNodeId:sibling.id, sessionId: sibling.sessionId, isTaken: true)
                        }
                    }
                }
                
            default:
                CoreDataUtils().updateNode(updatedNodeId:self.currentNode.id,sessionId: self.currentNode.sessionId, updatedValue: valueGiven, answerId: answerIdGiven, measurementDateTime: Date(), isTaken: true,isCompleted:true)
            }
        }
            

        
        /**
         Loads the child of a node. If not child is available, loads back its parent node's sibiling node
         - Parameters:
         - parentId : Node whose child or parent is to be loeaded
         
         - Throws: .
         
         - Returns: ViewModel.
         */
        func loadChildorParent(parentId:String)->ViewModel!{
            
            let nodes = CoreDataUtils().getUntakenChildNodesOfAssessmentParent(assessmentId:  self.assessmentId, sessionId:self.sessionId, parentId: parentId)
            //update  8-Oct-19
            let incompleteRootNodesCount = CoreDataUtils().getUntakenChildNodesOfAssessmentParent(assessmentId:  self.assessmentId, sessionId:self.sessionId, parentId: self.assessmentId).count
            
            let finishedRootNodesCount = CoreDataUtils().getTakenChildNodesOfAssessmentParent(assessmentId:  self.assessmentId, sessionId:self.sessionId, parentId: self.assessmentId).count
            //completedSessionsNumber: finishedRootNodesCount, totalSessionsNumber:(finishedRootNodesCount+incompleteRootNodesCount))
            if(nodes.count != 0 ){
                print("Loading: currentNode:\(self.currentNode.id)---node to be loaded:\(nodes[0].id)---parentid:\(parentId)")
                return HealthSessionViewController.ViewModel(assessmentId: self.assessmentId, sessionId:self.sessionId, currentParentId: parentId, currentNode: nodes[0], sessionName: self.sessionName,completedSessionsNumber:finishedRootNodesCount,totalSessionsNumber: (incompleteRootNodesCount + finishedRootNodesCount))
            }
            else{
                let parentNode = CoreDataUtils().getNode(id: parentId)
                print("Sibling Finder: currentNode:\(self.currentNode.id)---parentid:\(parentId)")
                guard parentNode != nil else{
                    /* indicating end of a branch - could be end of th tree if parentId == assessmentId
                       reason - the firt condition  that a root level node has parentId == assessmentId
                     */
                    CoreDataUtils().markSessionAsComplete(sessionId: self.sessionId, assessmentId: self.assessmentId)
                    return nil
                }
                return self.loadChildorParent(parentId: parentNode!.parentId!)
                
            }
            
            return nil
        }
        
    }
}

extension Notification.Name {
    static let deleteNodeNotification = Notification.Name("delete node")
}

