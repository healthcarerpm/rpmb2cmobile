//
//  MeasurementIdentifier.swift
//  Sample
//
//  Created by Himanshu on 4/7/19.
//  Copyright © 2019 Isham. All rights reserved.
//

import Foundation

class MeasurementIdentifier:NSObject{
    
    static let sharedInteractor: MeasurementIdentifier = {
        let sharedInteractor = MeasurementIdentifier()
        return sharedInteractor
    }()
    
    fileprivate override init() {
        super.init()
    }
    
    var systolicID:String?
    var diastolicID:String?
    var spo2ID:String?
    var pulseID:String?
    var bloodsugar:String?
    var temperatureID:String?
    var weightID:String?
    var isfromdashboardforTerms = true
    /* Start and end for validations*/
    var weightstart:Double?
    var weightEnd:Double?
    var diastolicStart:Int?
    var diastolicEnd:Int?
    var systolicStart:Int?
    var systolicEnd:Int?
    var Spo2start:Int?
    var temperatueEnd:Double?
    var temperatureStart:Double?
    var heartRateStart:Int?
    var heartrateEnd:Int?
    var bgStart:Double?
    var bgEnd:Double?
    
    /* Patient ID
     */
    var patientId:String?
    var messages:[Message]? = []
    var initialmessagecounter: Int = 0
   // var selfmessages:[SelfMessage] = []
    
    //repeating timer
    let repeatingTimer = RepeatingTimer(timeInterval: 30)
}
