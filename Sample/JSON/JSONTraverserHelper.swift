//
//  JSONTraverserHelper.swift
//  Sample
//
//  Created by Thejus Jose on 12/03/19.
//  Copyright © 2019 Isham. All rights reserved.
//

import Foundation
import Alamofire
import CoreData

class JSONTraverserHelper{
    
    
    static func doJSONTraversalSessionRequest(data:Data){
        
        let parentElementArray = JSONTraverser(data: data)
        for parentElement in parentElementArray {
            
            let assessmentArray = parentElement.get("Assessments")
            
            let patientId = parentElement.get("PatientId").string!
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "MM/dd/yyyy H:mm"
            var i = 0
            for assessment in assessmentArray {
               
                let serverDateTimeString = "\(assessment.get("Date").string ?? "") 00:00 "
                
                let utcDateTime = dateFormatter.date(from: serverDateTimeString)
                var sessionStartDatetopasstoserver = ""
                let serverStartDateTimeString = "\(assessment.get("Date").string ?? "") \(assessment.get("StartTime").string ?? "") "
                print(serverStartDateTimeString)
                let serverEndDateTimeString = "\(assessment.get("Date").string ?? "") \(assessment.get("EndTime").string ?? "") "
                let actualutcdatetimestring = Date()
                let localDateformatter = DateFormatter()
                localDateformatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
                let utctolocalDatetimeString = UTCToLocal(date:localDateformatter.string(from: actualutcdatetimestring) )
                print(utctolocalDatetimeString)
                if dateFormatter.date(from: serverStartDateTimeString)?.compare(dateFormatter.date(from: utctolocalDatetimeString)!) == ComparisonResult.orderedSame {
                    print("ignoring the case")
                    sessionStartDatetopasstoserver = UTCToLocalSession(date:utctolocalDatetimeString)
                }else if dateFormatter.date(from: serverStartDateTimeString)?.compare(dateFormatter.date(from: utctolocalDatetimeString)!) == ComparisonResult.orderedDescending {
                    print("date is behind")
                    
                    let component1  = Calendar.current.component(.day, from: (dateFormatter.date(from: serverStartDateTimeString)!))
                    let component2 = Calendar.current.component(.day, from: dateFormatter.date(from: utctolocalDatetimeString)!)
                    if component1 > component2 && (component1 - component2 == 1) && i == 0
                    {
                          sessionStartDatetopasstoserver = UTCToLocalSession(date: utctolocalDatetimeString)
                    }
                    else{
                    sessionStartDatetopasstoserver = UTCToLocalSession(date: serverStartDateTimeString)
                    }
                }else if dateFormatter.date(from: serverStartDateTimeString)?.compare(dateFormatter.date(from: utctolocalDatetimeString)!) == ComparisonResult.orderedAscending {
                    print(" date is ahead")
                    let component1  = Calendar.current.component(.day, from: (dateFormatter.date(from: serverStartDateTimeString)!))
                    let component2 = Calendar.current.component(.day, from: dateFormatter.date(from: utctolocalDatetimeString)!)
                    if component2 > component1 && (component1 - component2 == 1)
                    {
                        sessionStartDatetopasstoserver = UTCToLocalSession(date: utctolocalDatetimeString)
                    }
                    else{
                        sessionStartDatetopasstoserver = UTCToLocalSession(date: serverStartDateTimeString)
                    }
                    
                }
                
                let datecompoments = sessionStartDatetopasstoserver.components(separatedBy: " ")
                if datecompoments.count > 0{
                    sessionStartDatetopasstoserver = datecompoments[0] + " " + (assessment.get("StartTime").string ?? "")
                }
                
                let utcEndDateTime = UTCToLocalSession(date: serverEndDateTimeString)
                
                let utcAssessmentLasUpdateDateTime = dateFormatter.date(from: assessment.get("AssessmentLastUpdated").string!)
                print("sessionId-\(assessment.get("Id").string!)")
                CoreDataUtils().saveSessionFromServer(sessionId:assessment.get("Id").string!,assessmentId: assessment.get("AssessmentId").string!,date:utcDateTime, name:assessment.get("Name").string, patientId: patientId, startTime: dateFormatter.date(from: sessionStartDatetopasstoserver), endTime: dateFormatter.date(from: utcEndDateTime), assessmentLastUpdated: utcAssessmentLasUpdateDateTime)
             i = i + 1
            }
        }
    }
    
   
    
    static func doJSONTraversalAssessmentNodeRequest(data:Data,sessionId: String){

        let assessmentId:String = JSONTraverser(data: data).get("AssessmentId").string!
        let parentElement = JSONTraverser(data: data).get("Questions")

        let val = dfs(assessmentId:assessmentId,sessionId:sessionId,node: parentElement,parentId:assessmentId)
        
    }
    
    let nodeList = [Node]()
    
    
    static func dfs(assessmentId:String,sessionId:String, node:JSONTraverser?,parentId:String?) ->Bool{
        
        if(node != nil){
            var idx = 0
            for parent in node! where parent != nil {
                let nodeId:String = parent.get("Id").string!
                let nodeText:String? = parent.get("Text").string ?? nil
                let nodeValue:String? = parent.get("Value").string ?? nil
                let measurementTypeId:String? = parent.get("MeasurementTypeId").string ?? nil
                let nodeType:Node.NodeType = Node.NodeType(rawValue: parent.get("Type").string!.lowercased()) ?? Node.NodeType.option
                let alertType:Node.AlertType? = Node.AlertType(rawValue: (parent.get("Alert").string ?? Node.AlertType.colorless.rawValue).lowercased())
                idx += 1
                _ = idx
                var nodeOptions = [String]()
                var nodeChildren = [String]()
                //                switch nodeType {
                //                case "Radio" ,"Check":
                
                let childElement:JSONTraverser = parent.get("Childern")
                for child in childElement  where child != nil
                {
                    let childId:String = child.get("Id").string!
                    
                    if(child.get("Type").string! == "Option"){
                        //add option id to option array
                        nodeOptions.append(childId)
                    }
                    else{
                        //else add to childrenarray
                        nodeChildren.append(childId)
                    }
                    
                }
                print("---\(nodeText)---\(nodeOptions)---\(nodeChildren)")
                CoreDataUtils().saveNodeFromServer(id: nodeId, assessmentId: assessmentId, sessionId:sessionId, text: nodeText, value: nodeValue, nodeType: nodeType, alertType: alertType!, parentId: parentId,measurementTypeId:measurementTypeId, options: nodeOptions, children: nodeChildren)
                if(childElement != nil){
                    dfs(assessmentId:assessmentId,sessionId: sessionId, node:childElement, parentId: nodeId)
                }
                //endof parent loop
            }
            //endof if
        }
        return true
    }
    
}

