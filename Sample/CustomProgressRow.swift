//
//  CustomProgressRow.swift
//  Sample
//
//  Created by Thejus Jose on 08/10/19.
//  Copyright © 2019 Isham. All rights reserved.
//

import Eureka


// The custom Row also has the cell: CustomCell and its correspond value
public final class CustomProgressRow: Row<CustomProgressCell>, RowType {
    
    required public init(tag: String?) {
        super.init(tag: tag)
        // We set the cellProvider to load the .xib corresponding to our cell
        cellProvider = CellProvider<CustomProgressCell>(nibName: "CustomProgressCell")
    }
}

